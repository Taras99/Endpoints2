package com.example.text;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController

public class GreetingController {


    @GetMapping("/greeting")
    public String greeting(@RequestParam() String name) {

        return "Your name is " +name;
    }

    @GetMapping("/car/{color}")
    public car car_name(@PathVariable String color) {
        car carr = new car();
        carr.setColor(color);
        return carr;
    }

    int count = 0;

    @GetMapping("/count")
    public count car_count() {
        count++;
        count num = new count();
        num.setNumber(count);
        return num;
    }

    @GetMapping("/countdel")
    public count dellete_count() {
        count = 0;
        count num = new count();
        num.setNumber(count);
        return num;
    }
}


